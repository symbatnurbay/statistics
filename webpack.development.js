const webpack = require('webpack')  
var path = require('path')

var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: {
    client: [
      './client/index.js'
    ]
  },
  resolve: {
    extensions: ['.css', '.vue', '.js'],
    mainFiles: ['index'],
    alias: {
      vue: 'vue/dist/vue.js'
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: { 
              modules: true,
              camelCase: true,
              localIdentName: '[local]--[hash:base64:5]',
              importLoaders: 1
            }
          }
        ]
      }
    ]
	},
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './client/template.html'
    })
  ],
  devtool: '#source-map'
}
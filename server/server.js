import http from 'http'

import express from 'express'
import bodyParser from 'body-parser'
import socket from 'socket.io'

import mongoose from 'mongoose'

import Counter from './models/Counter'
import Users from './models/Users'

const app = express()

const server = http.Server(app)
var io = socket(server)

io.on('connection', (client) => {
  client.on('refresh', () => {
    Counter.findOne({}, function(err, counter){
      if(err)
        console.log(err)

      Users.find({}, (err, users) => {
        if(err)
          console.log(err)

        var myCounter = {
          registered: counter.registered, 
          full_registered: counter.full_registered,

          sent_register_msg: counter.sent_register_msg, 
          opened_register_msg: counter.opened_register_msg, 
          sent_forgot_msg: counter.sent_forgot_msg,
          opened_forgot_msg: counter.opened_forgot_msg, 
          sent_remind_msg: counter.sent_remind_msg,
          opened_remind_msg: counter.opened_remind_msg,
          sent_success_msg: counter.sent_success_msg,
          opened_success_msg: counter.opened_success_msg,
          sent_lastactive_msg: counter.sent_lastactive_msg,
          opened_lastactive_msg: counter.opened_lastactive_msg,
          sent_firstletter_msg: counter.sent_firstletter_msg,
          opened_firstletter_msg: counter.opened_firstletter_msg,
          sent_secondletter_msg: counter.sent_secondletter_msg,
          opened_secondletter_msg: counter.opened_secondletter_msg,
          sent_thirdletter_msg: counter.sent_thirdletter_msg,
          opened_thirdletter_msg: counter.opened_thirdletter_msg,
          sent_fourthletter_msg: counter.sent_fourthletter_msg,
          opened_fourthletter_msg: counter.opened_fourthletter_msg,
          sent_fifthletter_msg: counter.sent_fifthletter_msg,
          opened_fifthletter_msg: counter.opened_fifthletter_msg,


          all_clicks: counter.all_clicks,
          cvr_percent: counter.cvr_percent,
          all_wasted: counter.all_wasted,
          lid_cost: counter.lid_cost
        }

        var today = new Date()
        var temp = []
        temp = users.filter(item => {
          var tempDate = new Date(item.createdAt)
          return tempDate.toDateString() == today.toDateString()
        })
        myCounter.today_registered = temp.length

        users.map(function(item, index){
          if(item.trial == 1){
            myCounter.trial = myCounter.trial ? myCounter.trial+1 : 1
          } else if(item.trial == 0 && item.transaction.status == 'success') {
            myCounter.premium = myCounter.premium ? myCounter.premium+1 : 1
          } else {
            myCounter.trial_expired = myCounter.trial_expired ? myCounter.trial_expired+1 : 1
          }
        })
        client.emit('counter', myCounter)
      })
    })
  })
})

server.listen(8000, () => {
  console.log(`is now up and running on 8000`)
})

import index from './routes/index'

require('./models').connect('mongodb://localhost:27017/ilovepeople');
const webpack = require('webpack')
const webpackConfig = require('../webpack.development.js')
const compiler = webpack(webpackConfig)

app.use(require('webpack-dev-middleware')(compiler, {
  log: false,
  path: '../client/build/',
  heartbeat: 1000
}))


app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use(express.static('client/build'))

app.use('/api', index)

app.get('*', (req, res) => {
  res.redirect('/' + req.originalUrl)
})

app.use((err, req, res, next) => {
  console.error(err.stack)
  res.send(500, { message: err.message })
})

// Exporting server
export default { app }

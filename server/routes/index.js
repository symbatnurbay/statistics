import express from 'express'
const router = new express.Router();

import Counter from '../models/Counter'
import Users from '../models/Users'

router.get('/statistics', (req, res) => {
  Counter.findOne({}, function(err, counter){
    if(err)
      console.log(err)

    Users.find({}, (err, users) => {
      if(err)
        console.log(err)

      var myCounter = {
        registered: counter.registered, 
        full_registered: counter.full_registered,

        sent_register_msg: counter.sent_register_msg, 
        opened_register_msg: counter.opened_register_msg, 
        sent_forgot_msg: counter.sent_forgot_msg,
        opened_forgot_msg: counter.opened_forgot_msg, 
        sent_remind_msg: counter.sent_remind_msg,
        opened_remind_msg: counter.opened_remind_msg,
        sent_success_msg: counter.sent_success_msg,
        opened_success_msg: counter.opened_success_msg,
        sent_lastactive_msg: counter.sent_lastactive_msg,
        opened_lastactive_msg: counter.opened_lastactive_msg,
        sent_firstletter_msg: counter.sent_firstletter_msg,
        opened_firstletter_msg: counter.opened_firstletter_msg,
        sent_secondletter_msg: counter.sent_secondletter_msg,
        opened_secondletter_msg: counter.opened_secondletter_msg,
        sent_thirdletter_msg: counter.sent_thirdletter_msg,
        opened_thirdletter_msg: counter.opened_thirdletter_msg,
        sent_fourthletter_msg: counter.sent_fourthletter_msg,
        opened_fourthletter_msg: counter.opened_fourthletter_msg,
        sent_fifthletter_msg: counter.sent_fifthletter_msg,
        opened_fifthletter_msg: counter.opened_fifthletter_msg,


        all_clicks: counter.all_clicks,
        cvr_percent: counter.cvr_percent,
        all_wasted: counter.all_wasted,
        lid_cost: counter.lid_cost
      }

      var today = new Date()
      var temp = []
      temp = users.filter(item => {
        var tempDate = new Date(item.createdAt)
        return tempDate.toDateString() == today.toDateString()
      })
      myCounter.today_registered = temp.length

      users.map(function(item, index){
        if(item.trial == 1){
          myCounter.trial = myCounter.trial ? myCounter.trial+1 : 1
        } else if(item.trial == 0 && item.transaction.status == 'success') {
          myCounter.premium = myCounter.premium ? myCounter.premium+1 : 1
        } else {
          myCounter.trial_expired = myCounter.trial_expired ? myCounter.trial_expired+1 : 1
        }
      })
      res.status(200).send(myCounter)
    })
  })
})

router.post('/login', (req, res) => {
  var pwd = req.body.password
  var username = req.body.name

  if(username == 'admin' && pwd == 'admin'){
    res.status(200).end()
  } else {
    res.status(500).end()
  }
})

router.post('/update', (req, res) => {
  var myData = {}

  Counter.findOne({}, (err, counter) => {
    if(err)
      console.log(err)

    myData.all_clicks = req.body.all_clicks && req.body.all_clicks > 0 ? req.body.all_clicks : counter.all_clicks ? counter.all_clicks : 0
    myData.cvr_percent = req.body.cvr_percent && req.body.cvr_percent > 0 ? req.body.cvr_percent : counter.cvr_percent ? counter.cvr_percent : 0
    myData.all_wasted = req.body.all_wasted && req.body.all_wasted > 0 ? req.body.all_wasted : counter.all_wasted ? counter.all_wasted : 0
    myData.lid_cost = req.body.lid_cost && req.body.lid_cost > 0 ? req.body.lid_cost : counter.lid_cost ? counter.lid_cost : 0

    Counter.findOneAndUpdate({}, 
      {$set: {all_clicks: myData.all_clicks, 
              cvr_percent: myData.cvr_percent, 
              all_wasted: myData.all_wasted, 
              lid_cost: myData.lid_cost}}, (err, counter) => {
                if(err)
                  console.log(err)

                res.status(200).end()
              })
  })
})

module.exports = router

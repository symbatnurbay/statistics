const mongoose = require('mongoose')
const CounterSchema = new mongoose.Schema({
  registered: Number, 
  full_registered: Number,
  sent_register_msg: Number, 
  sent_forgot_msg: Number,
  sent_remind_msg: Number,
  sent_firstletter_msg: Number,
  sent_secondletter_msg: Number,
  sent_thirdletter_msg: Number,
  sent_fourthletter_msg: Number,
  sent_fifthletter_msg: Number,
  sent_success_msg: Number,
  sent_lastactive_msg: Number,

  opened_forgot_msg: Number, 
  opened_register_msg: Number, 
  opened_remind_msg: Number, 
  opened_firstletter_msg: Number,
  opened_secondletter_msg: Number,
  opened_thirdletter_msg: Number,
  opened_fourthletter_msg: Number,
  opened_fifthletter_msg: Number,
  opened_success_msg: Number,
  opened_lastactive_msg: Number,

  all_clicks: Number,
  cvr_percent: Number,
  all_wasted: Number,
  lid_cost: Number
})

module.exports = mongoose.model('Counter', CounterSchema)

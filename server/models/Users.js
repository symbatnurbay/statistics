const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  hash: String,
  salt: String,
  trial: Number, // 0 - trial expired, 1 - trial is active
  companyname: String,
  phone: String,
  last_login: String,
  address: String,
  first_name: String,
  last_name: String,
  subscribed: {
    type: Boolean,
    default: true
  },
  transaction: {
    id: String,
    amount: String,
    paidDate: {
      type: Date,
      default: Date.now()
    },
    status: String,
    months: String
  },
  last_active: Date,
  utm: {
    utm_source: String,
    utm_campaign: String,
    utm_keyword: String,
    utm_sourcersy: String,
    utm_medium: String
  }
},
{
  timestamps: true
}

)

module.exports = mongoose.model('Users', userSchema)

const mongoose = require('mongoose')

const msgSchema = new mongoose.Schema({
  user_id: {type: mongoose.Schema.ObjectId, ref: 'User' },
  opened_register_msg: {
    type: Boolean,
    default: false
  },
  opened_forgot_msg: {
    type: Boolean,
    default: false
  },
  opened_remind_msg: {
    type: Boolean,
    default: false
  },
  opened_firstletter_msg: {
    type: Boolean,
    default: false
  },
  opened_secondletter_msg: {
    type: Boolean,
    default: false
  },
  opened_thirdletter_msg: {
    type: Boolean,
    default: false
  },
  opened_fourthletter_msg: {
    type: Boolean,
    default: false
  },
  opened_fifthletter_msg: {
    type: Boolean,
    default: false
  },
  opened_success_msg: {
    type: Boolean,
    default: false
  },
  opened_lastactive_msg: {
    type: Boolean,
    default: false
  }
})

module.exports = mongoose.model('MessageStatus', msgSchema)

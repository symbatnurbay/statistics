import Vue from 'vue'
import VueRouter from 'vue-router'
import VueSocketio from 'vue-socket.io'
import router from './router'
import VueCookie from 'vue-cookie'

import App from './App.vue'

import Api from './api'

Vue.use(VueRouter)
Vue.use(VueCookie)
Vue.use(VueSocketio, 'http://localhost:8000')

const apiInstance = new Api()
Vue.prototype.$api = apiInstance

const app = new Vue({
  router,
  ...App
})

app.$mount('#app')
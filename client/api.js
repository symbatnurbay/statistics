import axios from 'axios'

export default function (option) {
  return {
    getStatistics: () => {
      return new Promise((resolve, reject) => {
        axios.get('/api/statistics')
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    login: (user) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/login', user)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    update: (myData) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/update', myData)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}

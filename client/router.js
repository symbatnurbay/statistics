import VueRouter from 'vue-router'

import Main from './Pages/Main'
import Dashboard from './Pages/Dashboard'
import Login from './Pages/Login'

const routes = [
  {
    path: '/',
    component: Main
  },
  {
    path: '/dashboard',
    component: Dashboard
  },
  {
    path: '/login',
    component: Login
  }
]

const router = new VueRouter({
  routes
})

export default router